package com.citi.training.git;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitExperimentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitExperimentsApplication.class, args);
	}

}
